package task

trait Increasable extends Accountable {
  def increase(amount: Currency)
}