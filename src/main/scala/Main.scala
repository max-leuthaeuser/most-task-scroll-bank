import solution.{Transaction, Bank, Account, Person}
import task.Currency

object Main extends App {
  val stan = Person("Stan")
  val brian = Person("Brian")

  val accForStan = new Account(Currency(10.0, "USD"))
  val accForBrian = new Account(Currency(0, "USD"))

  new Bank {
    stan play new Customer
    brian play new Customer
    accForStan play new CheckingsAccount
    accForBrian play new SavingsAccount

    +stan addAccount accForStan
    +brian addAccount accForBrian

    println("### Before transaction ###")
    println("Balance for Stan: " + accForStan.balance)
    println("Balance for Brian: " + accForBrian.balance)

    val transaction = new Transaction(Currency(10.0, "USD"))
    accForStan play new transaction.Source
    accForBrian play new transaction.Target

    // Defining a partOf relation between Transaction and Bank.
    // The transaction needs full access to registered/bound Accounts like
    // CheckingsAccount and SavingsAccount.
    transaction partOf this

    transaction play new TransactionRole execute()

    println("### After transaction ###")
    println("Balance for Stan: " + accForStan.balance)
    println("Balance for Brian: " + accForBrian.balance)

    println("Stan is playing the Customer role? " + (+stan).isPlaying[Customer])
    println("Brian is playing the Customer role? " + (+brian).isPlaying[Customer])
    println("Account for Stan is a CheckingsAccount? " + (+accForStan).isPlaying[CheckingsAccount])
    println("Account for Brian is a SavingsAccount? " + (+accForBrian).isPlaying[SavingsAccount])

    println(+stan listBalances())
    println(+brian listBalances())
  }
}