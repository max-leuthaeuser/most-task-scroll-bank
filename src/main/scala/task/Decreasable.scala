package task

trait Decreasable extends Accountable {
  def decrease(amount: Currency)
}