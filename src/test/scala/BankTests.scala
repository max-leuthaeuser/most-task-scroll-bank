import org.scalatest.FeatureSpec
import org.scalatest.GivenWhenThen
import org.scalatest.Matchers
import solution.{Transaction, Bank, Person, Account}
import task.{Accountable, Currency}

class BankTests extends FeatureSpec with GivenWhenThen with Matchers {
  info("Test spec for MOST SCROLL task 2.")

  feature("Your implementation of Account") {
    scenario("Initialization") {
      Given("an new Account")
      val a = new Account()
      Then("its initial balance should be 0.")
      a.balance shouldBe Currency(0, "USD")
    }

    scenario("Changing the balance") {
      Given("an new Account")
      val a = new Account()
      Then("changing its balance should work.")
      a.increase(Currency(10, "USD"))
      a.balance shouldBe Currency(10, "USD")
      a.decrease(Currency(10, "USD"))
      a.balance shouldBe Currency(0, "USD")
    }
  }

  feature("Your implementation of Person") {
    scenario("Initialization") {
      Given("an new Person")
      val p = Person("Pete")
      Then("its name should be set correctly.")
      p.name shouldBe "Pete"
    }
  }

  feature("Your implementation of Bank") {
    scenario("Transferring money") {
      Given("some Persons")
      val stan = Person("Stan")
      val brian = Person("Brian")

      And("some Accounts for them")
      val accForStan = new Account(Currency(10.0, "USD"))
      val accForBrian = new Account(Currency(0, "USD"))

      And("an new Bank")
      val b = new Bank {
        stan play new Customer
        brian play new Customer
        accForStan play new CheckingsAccount
        accForBrian play new SavingsAccount

        When("registering accounts to Customers")
        +stan addAccount accForStan
        +brian addAccount accForBrian
        Then("they should be added correctly")
        val acc1: List[Accountable] = (+stan).accounts
        val acc2: List[Accountable] = (+brian).accounts
        acc1 shouldBe List(accForStan)
        acc2 shouldBe List(accForBrian)

        And("their balances should be initialized correctly")
        accForStan.balance shouldBe Currency(10, "USD")
        accForBrian.balance shouldBe Currency(0, "USD")

        When("defining a new transaction")
        val transaction = new Transaction(Currency(10.0, "USD"))
        accForStan play new transaction.Source
        accForBrian play new transaction.Target

        // Defining a partOf relation between Transaction and Bank.
        // The transaction needs full access to registered/bound Accounts like
        // CheckingsAccount and SavingsAccount.
        transaction partOf this

        And("executing it")
        transaction play new TransactionRole execute()

        Then("the corresponding balances should be correct")
        accForStan.balance shouldBe Currency(0, "USD")
        accForBrian.balance shouldBe Currency(9, "USD")

        val c1: List[Currency] = +stan listBalances()
        val c2: List[Currency] = +brian listBalances()

        And("listed correctly as well.")
        c1 shouldBe List(Currency(0, "USD"))
        c2 shouldBe List(Currency(9, "USD"))
      }
    }
  }
}
